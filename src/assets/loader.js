import planeFly from './images/plane_sprite/png/Plane/Fly/*.png'
import planeDead from './images/plane_sprite/png/Plane/Dead/*.png'
import planeShoot from './images/plane_sprite/png/Plane/Shoot/*.png'
import bullet from './images/plane_sprite/png/Bullet/*.png'
import cloud from './images/clouds/*.png'
import mine from './images/mines/*.png'


export const planeFrames = {
    fly: Object.values(planeFly),
    dead: Object.values(planeDead),
    shoot: Object.values(planeShoot),
}

export const shotFrames = {
    shot: Object.values(bullet),
}

export const cloudFrames = {
    clouds: Object.values(cloud),
}
export const mineFrames = {
    mines: Object.values(mine),
}

export function GetSprite(name) {
    return new PIXI.AnimatedSprite(
      spriteNames[name].map((path) => PIXI.Texture.from(path))
    )
}