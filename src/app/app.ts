import { planeFrames, shotFrames, cloudFrames, mineFrames } from '../assets/loader';
import * as PIXI from 'pixi.js';
// import "pixi-plugin-bump";
import { PlaneFrames } from '../interfaces/PlaneFrames.interface';
import { BulletFrames } from '../interfaces/BulletFrames.interface';
import { bulletMove } from './bullet.move';
import { CloudFrames } from '../interfaces/Clouds.interface'
import { cloudMove } from './cloud.move';
import { MineFrames } from '../interfaces/MineFrames.interface';
import { mineMove } from './mine.move';


// Prepare frames
const playerFrames: PlaneFrames = planeFrames;
const cloudyFrames: CloudFrames = cloudFrames;
const bulletFrames: BulletFrames = shotFrames;
const minesFrames: MineFrames = mineFrames;

// Animations & sprites
const currentFrame: keyof PlaneFrames = 'fly';
const currentBullet: keyof BulletFrames = 'shot';
const currentCloud: keyof CloudFrames = 'clouds';
const currentMine: keyof MineFrames = 'mines';



export class GameApp {
    static GameOver: boolean = false;
    static Shoot: boolean = false;
    static FlyForward: boolean = false;
    static score: number = 0;

    static player: PIXI.AnimatedSprite;

    static Stage: PIXI.Container;
    // static bump = new PIXI.extras.Bump();
    private app: PIXI.Application;
    static ScoreText: PIXI.Text = new PIXI.Text("Score: ", {
        fontSize: 50,
        fill: "#000000",
        align: "center",
        stroke: "#aaaaaa",
        strokeThickness: 0
    });





    constructor(parent: HTMLElement, width: number, height: number) {

        this.app = new PIXI.Application({ width, height, backgroundColor: 0xFFFFF });
        parent.replaceChild(this.app.view, parent.lastElementChild); // Hack for parcel HMR

        // init Pixi loader
        let loader = new PIXI.Loader();

        // Add assets
        Object.keys(playerFrames).forEach(key => {
            loader.add(playerFrames[key]);
        });
        Object.keys(cloudyFrames).forEach(key => {
            loader.add(cloudyFrames[key]);
        })
        Object.keys(minesFrames).forEach(key =>
            loader.add(minesFrames[key]));
        // Load assets
        loader.load(this.onAssetsLoaded.bind(this));

        this.app.stage.sortableChildren = true;

    }




    loadCloud = () => {
        setInterval(() => {
            const cloud: PIXI.AnimatedSprite = new PIXI.AnimatedSprite(cloudFrames[currentCloud].map(path => PIXI.Texture.from(path)));


            cloud.x = Math.floor(Math.random() * (2500 - 1800 + 1) + 1800);
            cloud.y = Math.floor(Math.random() * (1800 - 800 + 1) + 500);
            cloud.height = 500;
            cloud.width = 450;
            cloud.animationSpeed = 0.2;
            cloud.anchor.set(0, 1);
            cloud.zIndex = -1;
            cloud.play();

            this.app.ticker.add((delta) => {

                cloudMove(cloud);
            })


            this.app.stage.addChild(cloud);


            setTimeout(() => {
                cloud.destroy()
            }, 4000)
        }, 1300)






    }
    loadPlayer = () => {
        const player = new PIXI.AnimatedSprite(playerFrames[currentFrame].map(path => PIXI.Texture.from(path)));
        GameApp.player = player;
        player.x = 450;
        player.y = 450;
        player.height = 170;
        player.width = 210;
        player.angle = 0;
        player['vx'] = 1;
        let playerSpeed = 45.5;


        player.anchor.set(0, 1);

        // player.anchor.set(0.5);
        player.animationSpeed = 0.2;
        player.play();

        window.onkeydown = (ev: KeyboardEvent) => {
            const up = 'ArrowUp';
            const down = 'ArrowDown';
            const left = 'ArrowLeft';
            const right = 'ArrowRight';


            if (ev.key === right) {

                player.x += playerSpeed;
            }

            if (ev.key === left) {
                player.x -= playerSpeed
            }
            if (ev.key === up) {
                player.y -= playerSpeed;
            }

            if (ev.key === down) {
                player.y += playerSpeed;
            }
            if (ev.key === ' ' && !ev.repeat) {

                this.playerShoot(player);
            }
        }



        this.app.stage.addChild(player);

    }
    playerShoot = (player: PIXI.AnimatedSprite) => {
        const bullet: PIXI.AnimatedSprite = new PIXI.AnimatedSprite(bulletFrames[currentBullet].map(path => PIXI.Texture.from(path)));
        bullet.width = 75;
        bullet.height = 75;
        bullet.play();
        bullet.x = player.x + 155;
        bullet.y = player.y - 83;
        this.app.stage.addChild(bullet);
        this.app.ticker.add((delta) => {
            bulletMove(bullet);
        })
    }
    spawnMines = () => {

        setInterval(() => {
            const mine = new PIXI.AnimatedSprite(mineFrames[currentMine].map(path => PIXI.Texture.from(path)));
            mine.x = Math.floor(Math.random() * (2500 - 1800 + 1) + 1800);
            mine.y = Math.floor(Math.random() * (1500 - 100 + 1) + 100);
            mine.height = 75;
            mine.width = 75;
            mine.animationSpeed = 0.2;
            mine.anchor.set(0, 1);
            mine.zIndex = 1;
            mine.play();
            this.app.ticker.add((delta) => {
                mineMove(mine);
            })
            this.app.stage.addChild(mine);
            setTimeout(() => {
                mine.destroy()
            }, 4000)
        }, 1300)
    }

    // collisionCheck = (a: PIXI.AnimatedSprite, b: PIXI.AnimatedSprite) => {
    //     GameApp.bump.hit(a, b, true, false,true,() => {
    //         GameApp.ScoreText.text = "Game Over";
    //         
    //     });
    // }

    displayScore = () => {
        this.app.ticker.add(() => {
            GameApp.ScoreText.text =
                "Score: " +
                Math.ceil(GameApp.score);
            this.app.stage.addChild(GameApp.ScoreText);
            GameApp.score += 0.05;
        })

    }



    private onAssetsLoaded() {

        if (!GameApp.GameOver) {
            this.loadCloud();
            this.loadPlayer();
            this.spawnMines();
            this.displayScore();

            // this.collisionCheck(GameApp.player, GameApp.mine);


        } else {
            
        }

    }
}





















