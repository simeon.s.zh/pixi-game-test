export interface PlaneFrames {
    fly: string[];
    dead: string[];
    shoot: string[];
}